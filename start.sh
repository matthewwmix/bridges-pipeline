#!/bin/bash

docker stop pipeline
docker rm pipeline
docker run -d --network host --name pipeline -p 5432:5432 -p 8989:8989 -p 9122:9122 -v /opt/collector/bridges-pipeline:/host_storage -v /opt/collector/bridges-pipeline/metrics.json:/pipeline/metrics.json -v /opt/collector/bridges-pipeline/pipeline.conf:/pipeline/pipeline.conf -v /opt/collector/bridges-pipeline/pem_file:/pipeline/pem_file bridges/pipeline:v1.0.0
