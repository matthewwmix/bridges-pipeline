FROM centos:7

RUN yum install wget vim -y

RUN wget https://github.com/cisco/bigmuddy-network-telemetry-pipeline/archive/v1.0.0.tar.gz -O /v1.0.0.tar.gz && \
    mkdir /pipeline && \
    tar -xzvf v1.0.0.tar.gz && \
    mv bigmuddy-network-telemetry-pipeline-1.0.0/bin/pipeline /pipeline/pipeline && \
    rm -f /v1.0.0.tar.gz && \
    rm -rf /bigmuddy-network-telemetry-pipeline-1.0.0/

COPY files/startup.sh /pipeline/startup.sh

# Expose pipeline
EXPOSE 5432

WORKDIR /pipeline
CMD ["/pipeline/startup.sh"]
