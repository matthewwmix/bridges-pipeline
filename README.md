# Description
pipeline is a tool from cisco that can collect ST (streaming telemetry) data. See their [gitlab](https://github.com/cisco/bigmuddy-network-telemetry-pipeline) page for more details.

## Other interesting references
https://developer.cisco.com/codeexchange/github/repo/cisco/bigmuddy-network-telemetry-pipeline/  
https://blogs.cisco.com/sp/introducing-pipeline-a-model-driven-telemetry-collection-service  
https://xrdocs.io/telemetry/tutorials/2018-06-04-ios-xr-telemetry-collection-stack-intro/  

# if=mib oid to yang models
https://xrdocs.io/telemetry/tutorials/2016-10-13-using-model-driven-telemetry-mdt-for-if-mib-data/

This presentation had some cool tools that I found useful for digging around in in YANG land:
https://www.ciscolive.com/c/dam/r/ciscolive/emea/docs/2019/pdf/DEVNET-2415.pdf

In particular I would highly recommend having pyang installed for whenever you want to review the available yang models and build out your metrics.json
```bash
git clone https://github.com/YangModels/yang.git
cd yang/vendor/cisco/xr/631

pip install pyang
pyang -f tree Cisco-IOS-XR-Ethernet-SPAN-cfg.yang
```

# Building
```bash
./build.sh
```

# Usage
```bash
# The following script will start the pipeline container and expose port 5432 on the localhost to collect ST metrics on. 
# It will instruct pipeline to use the pipeline.conf in this directory. See the pipeline.conf for more details.
./start.sh
```
